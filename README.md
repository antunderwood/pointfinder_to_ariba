# Convert pointfinder_db to Ariba
This script will take variants encoded in the [pointfinder_db](https://bitbucket.org/genomicepidemiology/pointfinder_db) produced by the team at the Center for Genomic Epidemiology (DTU) and convert it to a format that the [Ariba software](https://github.com/sanger-pathogens/ariba) for detecting AMR determinants can use

### Requirements
  - Python3
  - Biopython
  - Ariba

### Running the script
1. First download the pointfinder database

    ```
    git clone https://bitbucket.org/genomicepidemiology/pointfinder_db.git
    ```
2. Download this code
    ```
    git clone https://gitlab.com/antunderwood/pointfinder_to_ariba.git
    ```
3. Run the code
  - Usage
    ```
    usage: pointfinder_to_ariba.py [-h] -p POINTFINDER_DB_DIRECTORY -o
                                OUTPUT_DIRECTORY

    A script to convert a pointfinder_db to a custom ariba database

    optional arguments:
    -h, --help            show this help message and exit
    -p POINTFINDER_DB_DIRECTORY, --pointfinder_db_directory POINTFINDER_DB_DIRECTORY
                            path to directory containing pointfinder db
    -o OUTPUT_DIRECTORY, --output_directory OUTPUT_DIRECTORY
                            path to directory for outputs
    ```
  - Example

    If you have performed the previous two steps in the same directory an example command would be
    ```
    cd pointfinder_to_ariba
    python3 pointfinder_to_ariba.py -p ../pointfinder_db -o ../ariba_databases
    ```
    The ariba database for each species will be found in the `ariba_databases` directory

### Considerations
1. Ariba does not allow the following

    - Indels
    - Truncations
    
    Therefore these are not converted from pointfinder to Ariba
2. There are a few mistakes in pointfinder and these are corrected

    - Sequences that should start from a ATG/GTG start codon are corrected
    - Some amino acids, gene names and positions need correction and this occurs using the information found in [this file](functions/pointfinder_corrections.yml) 
