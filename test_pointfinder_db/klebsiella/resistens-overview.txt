#Gene_ID	Gene_name	Codon_pos	Ref_nuc	Ref_codon	Res_codon	Resistance	PMID	Mechanism	Notes	Required_mut
acrR	acrR	121	GGT	G	*	tigecycline	27219271		Under development. Phenotype should be used with caution.	
acrR	acrR	382	CAGGCCCAGCGCCAG	del	QAQRQ	norfloxacin,ciprofloxacin	12936981		Under development. Phenotype should be used with caution.	
ompK36	ompK36	49	GAC	D	S	cephalosporins	25245001		Under development. Phenotype should be used with caution.
ompK36	ompK36	183	GGC	G	D	cephalosporins	25245001		Under development. Phenotype should be used with caution.	
ompK37	ompK37	712	-	ins	TERY	carbapenem	22282462		Under development. Phenotype should be used with caution.	