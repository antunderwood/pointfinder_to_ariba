#!/usr/bin/env python3
import argparse
import os
from functions.per_species_processes import make_ariba_files_for_all_species

class ExtendedParser(argparse.ArgumentParser):
    def is_valid_file(self, parser, arg):
        if not os.path.isfile(arg):
            parser.error("The file %s does not exist!" % arg)
        else:
            return arg
    
    def is_valid_directory(self, parser, arg):
        if not os.path.isdir(arg):
            parser.error("The directory %s does not exist!" % arg)
        else:
            return arg

def parse_arguments():
    description = """
    A script to convert a pointfinder_db to a custom ariba database
    """
    parser = ExtendedParser(description = description)

    parser.add_argument("-p", "--pointfinder_db_directory",
                        help="path to directory containing pointfinder db",
                        type=lambda x: parser.is_valid_directory(parser, x),
                        required = True)
    parser.add_argument("-o", "--output_directory",
                    help="path to directory for outputs",
                    type=lambda x: parser.is_valid_directory(parser, x),
                    required = True)
    
    return parser.parse_args()

def main():
    args = parse_arguments()

    make_ariba_files_for_all_species(args.pointfinder_db_directory, args.output_directory)

if __name__ == "__main__":
    main()