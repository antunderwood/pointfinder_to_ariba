import os
import csv
import subprocess
from functions.sequence_processing_functions import get_gene_sequences, gene_sequence_based_on_start_position
from functions.point_mutation_functions import get_point_mutations

def make_ariba_files_for_all_species(pointfinder_db_directory, output_directory):
    """
    Process the species directories in a  data pointfinder_db directory
    Args:
        pointfinder_db_directory (str): path to pointfinder_db directory 
        output_directory (str): path to output directory
    Returns:
        None
    """
    for species in sorted(os.listdir(pointfinder_db_directory)):
        if (os.path.isdir(os.path.join(pointfinder_db_directory, species))
            and not species.startswith('.')
            and not species.startswith('_')
            and not species.startswith('test')
            and not species.startswith('plasmodium')
            ):
            print(species)
            resistance_metadata_file = os.path.join(pointfinder_db_directory, species, 'resistens-overview.txt')
            gene_sequences = get_gene_sequences(pointfinder_db_directory, species)
            point_mutations = get_point_mutations(species, resistance_metadata_file, gene_sequences)
            write_species_metadata(output_directory, species, point_mutations)
            write_species_fasta(output_directory, species, point_mutations, gene_sequences)
            make_ariba_database(output_directory, species)

def write_species_metadata(output_directory, species, point_mutations):
    """
    Write out the tsv metadata file that Ariba requires for prepareref
    Args:
        output_directory (str): path to output directory
        species (str): species name
        point_mutations (dict): dictionary of point mutations made using the make_point_mutation_dict function

    Returns:
        None
    """
    if not os.path.exists(os.path.join(output_directory, species)):
        os.makedirs(os.path.join(output_directory, species))
    with open(os.path.join(output_directory, species, f'{species}_metadata.tsv'), 'w') as f:
        fieldnames = ['name', 'is_coding', 'variants_only', 'change', 'group', 'info']
        writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t')
        variant_counter = 0
        for gene in sorted(point_mutations.keys()):
            for point_mutation in point_mutations[gene]:
                variant_counter += 1
                # ariba wants a seqence name with a dot in it add .1
                point_mutation['name'] = f"{point_mutation['name']}.1"
                writer.writerow(point_mutation)
        print(f'{variant_counter} Ariba compatible variants written to metadata file\n')



def write_species_fasta(output_directory, species, point_mutations, gene_sequences):
    """
    Write out the fasta file that Ariba requires for prepareref
    Args:
        output_directory (str): path to output directory
        species (str): species name
        point_mutations (dict): dictionary of point mutations made using the make_point_mutation_dict function
        gene_sequences (dict): dictionary of gene sequences using the get_gene_sequences function

    Returns:
        None
    """
    with open(os.path.join(output_directory, species, f'{species}.fas'), 'w') as f:
        for gene in sorted(point_mutations.keys()):
            f.write(f'>{gene}.1\n')
            f.write(gene_sequence_based_on_start_position(gene_sequences, gene))
            # check for new line
            if not gene_sequence_based_on_start_position(gene_sequences, gene).endswith('\n'):
                f.write('\n')

def make_ariba_database(output_directory, species):
    """
    Make Ariba database from metadata and fasta file
    Args:
        output_directory (str): path to output directory
        species (str): species name
    """
    fasta_file = os.path.join(output_directory, species, f'{species}.fas')
    metadata_file = os.path.join(output_directory, species, f'{species}_metadata.tsv')
    ariba_database_path = os.path.join(output_directory, species, f'{species}_db')
    if os.path.isdir(ariba_database_path):
        print(f'{ariba_database_path} already exists. Please remove this directory if you want to regenerate it')
        exit()
    command = ['ariba', 'prepareref', '--no_cdhit', '-f', fasta_file, '-m', metadata_file, ariba_database_path]
    subprocess.run(command)

