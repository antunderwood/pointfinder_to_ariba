import os
import csv
import re
import glob
import yaml

# Global pointfinder_corrections file
with open(os.path.join(os.path.dirname(__file__), 'pointfinder_corrections.yml')) as corrections_yml:
    POINTFINDER_CORRECTIONS = yaml.load(corrections_yml, Loader=yaml.SafeLoader)

def get_position_based_on_start_position(gene, position, is_coding):
    """
    Based on the start position correct the position so that it matches the sequence
    Args:
        gene(dict): gene dict with keys sequence and start_position extracted from the gene_sequences dict
        position (int): position encoded in resistens-overview.txt
        is_coding (int): 0 or 1 equivalent to False and True
    Returns:
    corrected_position (int): The new position after start position has been considered
    """
    if is_coding == 0:
        # check if it's a promoter mutation
        if position < 0:
            corrected_position = len(gene['sequence']) + position + 1
        else:
            corrected_position = position - gene['start_position'] + 1
    else:
        corrected_position = int(position - (gene['start_position']/3)) + 1
    
    return corrected_position

def get_corrected_gene_id_or_position_or_amino_acid(species, gene_id, reference_wt_codon, position, variant):
    """
    correct position, amino acid or gene_id based on mistakes that have been found and recorded in pointfinder_corrections.yml
    Args:
        species (str): species name
        gene_id (str): name of the gene (labelled as gene_id in resistens-overview.txt)
        reference_wt_codon (str): the reference WT sensitive codon seen in the sequence encoded in resistens-overview.txt)
        position (int): the position encoded in  resistens-overview.txt)
        variant (str): the nucleotide or amino acid variant causing resistance and described in resistens-overview.txt)
    Returns:
        (corrected_gene_id, corrected_aa, corrected_position) (tuple): the corrected information or no changes if the position is correct in resistens-overview.txt)
    """
    try:
        if POINTFINDER_CORRECTIONS[species][gene_id][f'{reference_wt_codon}{position}{variant}']:
            if 'corrected_aa' in POINTFINDER_CORRECTIONS[species][gene_id][f'{reference_wt_codon}{position}{variant}']:
                corrected_aa = POINTFINDER_CORRECTIONS[species][gene_id][f'{reference_wt_codon}{position}{variant}']['corrected_aa']
            else:
                corrected_aa = reference_wt_codon
            if 'corrected_position' in POINTFINDER_CORRECTIONS[species][gene_id][f'{reference_wt_codon}{position}{variant}']:
                corrected_position = POINTFINDER_CORRECTIONS[species][gene_id][f'{reference_wt_codon}{position}{variant}']['corrected_position']
            else:
                corrected_position = position
            if 'corrected_gene_id' in POINTFINDER_CORRECTIONS[species][gene_id][f'{reference_wt_codon}{position}{variant}']:
                corrected_gene_id = POINTFINDER_CORRECTIONS[species][gene_id][f'{reference_wt_codon}{position}{variant}']['corrected_gene_id']
            else:
                corrected_gene_id = gene_id
            return corrected_gene_id, corrected_aa, corrected_position
    except KeyError:
        return gene_id, reference_wt_codon, position
