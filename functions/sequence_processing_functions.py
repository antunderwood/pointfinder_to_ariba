import glob
import os
import re
from Bio.SeqIO.FastaIO import SimpleFastaParser

def get_sequence(fasta_path):
    """
    Get the sequence from a fasta file
    Args:
        fasta_path (str): path to fasta file
    Returns:
        sequence (str): the sequence
    """
    with open(fasta_path) as seq_handle:
        _, sequence = next(SimpleFastaParser(seq_handle))
    return sequence

def get_start_position(sequence):
    """
    Get the start position of the first ATG assuming in frame
    Args:
        sequence (str): DNA sequence containing coding sequences
    Returns:
        first_atg_position (int): the start position or None
    """
    for i in range(0, len(sequence),3):
        codon = sequence[i:i+3].upper()
        if codon == 'ATG' or codon == 'GTG':
            first_atg_position = i + 1
            return first_atg_position
    return None

def gene_sequence_based_on_start_position(gene_sequences, gene):
    """
    Get the sequence based on the ATG/GTG start
    Args:
        gene_sequences (dict): dictionary of gene sequences using the get_gene_sequences function
        gene (str): name of gene
    Returns:
        sequence (str): the sequence
    """
    start_position = gene_sequences[gene]['start_position']
    sequence = gene_sequences[gene]['sequence'][start_position - 1:]
    return sequence

def get_gene_sequences(pointfinder_db_directory, species):
    """
    Make a dictionary containing gene sequences and start positions
    Args:
        pointfinder_db_directory (str): path to pointfinder_db directory 
        species (str): species name
    Returns:
        gene_sequences (dict): a dictionary of genes and promoter names as keys and sequence and start position
                            { gene_name : { sequence : X, start_position: Y}, ...}  
    """
    gene_sequences = {}
    for fasta_file in glob.glob(f'{pointfinder_db_directory}/{species}/*.fsa'):
        gene_name = os.path.basename(fasta_file).replace('.fsa', '')
        sequence = get_sequence(fasta_file)
        # if gene contains the text _promoter_size then it should be split into the promoter and coding sequence
        if '_promoter_size_' in gene_name:
            promoter_size = int(re.match(r'(.+)_promoter_size_(\d+)bp', gene_name).groups()[1])
            gene_name = re.match(r'(.+)_promoter_size_(\d+)bp', gene_name).groups()[0]
            promoter_sequence = sequence[0:promoter_size]
            gene_sequences[f'{gene_name}_promoter'] = {'sequence': promoter_sequence, 'start_position': 1} 
            gene_sequence = sequence[promoter_size:]
            start_position = get_start_position(gene_sequence)
            if start_position: # check not None since in some cases these contain promoter and the gene. Other times just the promoter
                gene_sequences[gene_name] = {'sequence': gene_sequence, 'start_position': start_position} 
        else:
            # correct starting positions for start ATG if coding non rRNA gene
            if re.match(r'^\d+S', gene_name):
                start_position = 1
            else:
                start_position = get_start_position(sequence)
            
            gene_sequences[gene_name] = {'sequence': sequence, 'start_position': start_position} 
    return gene_sequences
