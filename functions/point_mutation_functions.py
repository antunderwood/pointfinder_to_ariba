import re
import csv

from functions.position_correction_functions import get_position_based_on_start_position , get_corrected_gene_id_or_position_or_amino_acid
def make_point_mutation_dict(species, metadata_row, gene_sequences):
    """
    Make a list of mutations for a gene where elements are a dict with key:
        name, is_coding, variants_only, change, group, info
    Args:
        species (str): species name
        metadata_row (str): a row from the resistens-overview.txt)
        gene_sequences (dict):  dictionary of gene sequences using the get_gene_sequences function
    Returns:
        gene_id, variant_list (tuple): gene name as a string and a list of variants
    """
    variant_list = []
    gene_id = metadata_row['#Gene_ID']
    # get position 
    position = int(metadata_row['Codon_pos'])
    # edit gene_id if promoter sequence
    if '_promoter_size_' in gene_id:
        gene_name_prefix = re.match(r'(.+)_promoter_size_(\d+)bp', gene_id).groups()[0]
        if position < 0:
            gene_id = f'{gene_name_prefix}_promoter'
        else:
            gene_id = gene_name_prefix

    # find out if coding
    reference_wt_nucleotide = metadata_row['Ref_nuc']
    if len(reference_wt_nucleotide) == 3:
        is_coding = 1
    else:
        is_coding = 0
    
    # get reference codon
    reference_wt_codon = metadata_row['Ref_codon']

    # check not an indel
    if not re.match('[GATC]', metadata_row['Ref_nuc'], re.IGNORECASE):
        return gene_id, variant_list
    # check not del
    if not re.match('^[A-Z]$', metadata_row['Ref_codon'], re.IGNORECASE):
        return gene_id, variant_list
    
    # get info fields
    info_fields = []
    for field in ['Resistance', 'Mechanism', 'Notes', 'PMID']:
        if metadata_row[field]:
            info_fields.append(f'{field}: {metadata_row[field]}')
    
    info = '. '.join(info_fields) 
    
    # determine variants
    variants = metadata_row['Res_codon'].split(',')
    for variant in variants:
        if not re.match('[A-Z]', variant, re.IGNORECASE):
            continue
        # test if in corrections
        corrected_gene_id, corrected_aa, corrected_position = get_corrected_gene_id_or_position_or_amino_acid(species, gene_id, reference_wt_codon, position, variant)
        # correct for ATG at position other than 1
        corrected_position = get_position_based_on_start_position(gene_sequences[gene_id], corrected_position, is_coding)
        variant_list.append({
            'name': corrected_gene_id,
            'is_coding': is_coding,
            'variants_only': 1,
            'change': f'{corrected_aa}{corrected_position}{variant}',
            'group' : corrected_gene_id,
            'info' : info
        })
    return gene_id, variant_list

def get_point_mutations(species, resistance_metadata_file, gene_sequences):
    """
    get point mutations from the resistens-overview.txt file and pull out those that Ariba can use
    Args:
        species (str): species name
        resistance_metadata_file (str): name of the species resistance file from pointfinder_db 
        gene_sequences (dict):  dictionary of gene sequences using the get_gene_sequences function
    Returns:
        point_mutations(dict): dictionary of point mutations - key is gene name and value is a list of 
                               dicts with values name, is_coding, variants_only, change, group, info
    """
    point_mutations = {}
    with open(resistance_metadata_file) as tsvfile:
        reader = csv.DictReader(tsvfile, delimiter='\t')
        variant_counter = 0
        for row in reader:
            if row['#Gene_ID'].startswith("#"): # comment line
                continue
            variant_counter += len(row['Res_codon'].split(','))
            gene_id, variant_list = make_point_mutation_dict(species, row, gene_sequences)
            for variant in variant_list:
                if gene_id in point_mutations:
                    point_mutations[gene_id].append(variant)
                else:
                    point_mutations[gene_id] = [variant]
        print(f'{variant_counter} variants found in pointfinder_db metadata file')
    return point_mutations